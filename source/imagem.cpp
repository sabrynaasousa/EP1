#include "imagem.hpp"
#include <iostream>
#include <fstream>
#include <string>
#include <stdlib.h>
#include <string.h>
#include <sstream>

using namespace std;

imagem::imagem ()
{
	altura = 0;
	largura = 0;
	ncinza = 0;
	pixels = 0;
}

void imagem::lendoimagem ()
{
	int altura, largura, ncinza;
	char nmagico[3], altura1[4], largura1[5], ncinza1[10], comentario[100];
	char teste[3] = "P5";
	unsigned char *imagem;
	int i, j;

	pixels = new int [512*512+513];

	ifstream file ("lena.pgm");
	
	//lendo numero magico
	file.getline (nmagico, 3);
	
	//testando se e do tipo pgm
	if (strcmp(nmagico, teste) != 0)
	{
   		cout << "A imagem nao e do tipo pgm" << endl;
	        exit(1);
	}	


	file.getline (comentario, 100);
	file.get (altura1, 4);
	file.getline (largura1, 5);
	file.getline (ncinza1, 10);

	altura = atoi(altura1);
	largura = atoi(largura1);
	ncinza = atoi(ncinza1);
	
	this -> altura = altura;
	this -> largura = largura;
	this -> ncinza = ncinza;

	
	imagem = (unsigned char *) new unsigned char [altura*largura + 513];	  
	file.read( reinterpret_cast<char *>(imagem), (altura*largura)*sizeof(unsigned char));  

	file.close();

    long long val;

	for(i = 0; i < altura; i++)
	{
		for(j = 0; j < largura; j++)
	       {
			  val = (int)imagem[i * largura + j];
              pixels[i * largura + j] = val;  	          
	 		}

	}
}
	
int imagem::getaltura()
{
	return altura;
}

int imagem::getlargura ()
{
	return largura;
}

int imagem::getncinza()
{
	return ncinza;
}

void imagem::salvarimagem ()
{
	int i, j;
	unsigned char *imagem;
	ofstream outfile ("lenasalvo.pgm");

	imagem = (unsigned char *) new unsigned char [512*512 + 513];	 

    long long val;

	for(i = 0; i < 512; i++)
	{
		for(j = 0; j < 512; j++)
	    {
			 val = pixels[i * 512 + j];
             imagem[i * 512 + j] = (unsigned char)val;  	          
		}
	}

	outfile << "P5" << endl;
    outfile << "512 512" << endl;
	outfile << "#..."<<endl;
    outfile << 255 << endl;

	outfile.write( reinterpret_cast<char *>(imagem), (512*512)*sizeof(unsigned char)); 

	cout << "Arquivo criado" << endl;
	outfile.close();
}

void imagem::negativoimagem ()
{
	int i, j;
	unsigned char *imagem;
	ofstream outfile ("lenanegativo.pgm");

	imagem = (unsigned char *) new unsigned char [512 * 512 + 513];	 

    long long val;

	for(i = 0; i < 512; i++)
	{
		for(j = 0; j < 512; j++)
	    {
			 val = 255 - pixels[i * 512 + j];
             imagem[i * 512 + j] = (unsigned char)val;  	          
		}
	}

	outfile << "P5" << endl;
    outfile << "512 512" << endl;
	outfile << "#..."<<endl;
    outfile << 255 << endl;

	outfile.write( reinterpret_cast<char *>(imagem), (512 * 512)*sizeof(unsigned char)); 

	outfile.close();
}

int imagem::getpixels (int linhas, int colunas)
{
	int sizeY = getaltura();
	return pixels[linhas * sizeY + colunas];
} 

void imagem::setpixels (int linhas, int colunas, int valor)
{
	int sizeY = getlargura();
	pixels[linhas * sizeY + colunas] = valor;
} 















