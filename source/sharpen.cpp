#include "sharpen.hpp"
#include "filtro.hpp"


using namespace std;

Sharpen::Sharpen(){	
	
}

void Sharpen::aplica(imagem &umaimagem, int div, int size) {
		int sharpen[] = {0, -1, 0, -1, 5, -1, 0, -1, 0};

		int altura, largura, valor, i,j;
		
		altura = 512;
		largura =512;
		int *m = new int[altura*largura];
		
		for (i=size/2; i < altura-size/2; i++)
		{
			for (j = size/2; j < (largura - size/2); j++)
			{
				valor = 0;
				for(int x = -1; x <= 1; x++)
				{		
					for(int y = -1; y <= 1; y++)
					{		
						valor += sharpen[(x + 1)+ size*(y + 1)] *
					    umaimagem.getpixels(i + x, y + j);
					}
				}
				
				valor /=div;
				
			
					
				valor= valor < 0 ? 0 : valor;
				valor=valor >255 ? 255 : valor;
				
				m[i+largura*j] = valor;
			}
		}
		
		for(i = 0; i < altura; i++)
			for(j = 0; j < largura; j++)
				umaimagem.setpixels(i, j, m[i + largura * j]);

}
