#include "imagem.cpp"
#include "filtro.cpp"
#include "sharpen.cpp"
#include "smooth.cpp"


int main ()
{
	imagem lena;
	lena.lendoimagem();
	int opcao;
	Smooth umSmooth;
	Sharpen umSharpen;

	cout << "Escolha um filtro:" << endl;
	cout << "1- Negativo" << endl << "2- Smooth" << endl << "3- Sharpen" << endl;
	
	cout << "Opcao: ";
	cin >> opcao;

	if(opcao == 1)
	{
		lena.negativoimagem();
		cout<< "A Imagem com o filtro foi salva!"<<endl;
	}
	else if(opcao == 2)
	{		
		umSmooth.setDiv(9);
		umSmooth.setSize(3);
		umSmooth.aplica(lena,umSmooth.getDiv(), umSmooth.getSize());
		lena.salvarimagem();
		cout<< "A Imagem com o filtro foi salva!"<<endl;
	}
	else if(opcao == 3)
	{
		umSharpen.setDiv(1);
		umSharpen.setSize(3);
		umSharpen.aplica(lena, umSharpen.getDiv(), umSharpen.getSize());
		lena.salvarimagem();
		cout<< "A Imagem com o filtro foi salva!"<<endl;
	}
	else
	{
		cout<< "Opcao invalida"<<endl;
	}
	
		
	return 0;
}



