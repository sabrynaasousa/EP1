#ifndef IMAGEM_H
#define IMAGEM_H
#pragma
#include <iostream>
	 
class imagem 
{
	private:
		int altura;
		int largura;
		int ncinza;
		int *pixels; 

	public :
		imagem ();
		imagem (int altura, int largura, int ncinza);
		void lendoimagem ();
		void salvarimagem ();
		void negativoimagem ();
		int getaltura ();
		int getlargura ();
		int getncinza ();
		int getpixels (int linhas, int colunas);
		void setpixels (int linhas, int colunas, int valor);

};

#endif
