#ifndef SHARPEN_H
#define SHARPEN_H

#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <stdlib.h> 

#include "imagem.hpp"
#include "filtro.hpp"

using namespace std;

class Sharpen : public Filtro {
	public:
		Sharpen();
		
		void aplica(imagem &umaimagem, int div, int size);
};

#endif
