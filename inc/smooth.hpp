#ifndef SMOOTH_H
#define SMOOTH_H

#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <stdlib.h> 

#include "imagem.hpp"
#include "filtro.hpp"
using namespace std;

class Smooth : public Filtro{
	public:
		Smooth();
		
		void aplica(imagem &umaimagem, int div, int size);
};

#endif
