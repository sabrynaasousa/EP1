#ifndef FILTRO_H
#define FILTRO_H
#pragma
#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <stdlib.h> 

#include "imagem.hpp"

using namespace std;

class Filtro {
	private:
		int div;
		int size;
	public:
		Filtro();
		void setDiv(int div);
		void setSize(int size);
		int getDiv();
		int getSize();
		virtual void aplica(){return;};
};

#endif
